package com.company;

import com.company.data.Generator;
import com.company.model.Shop;

import java.util.Scanner;


public class Task {

    public void run() {
        Shop[] shops = Generator.generate ();

        Scanner scan = new Scanner ( System.in );

        System.out.printf ( "Введите название магазина для поиска в нем самого дешевого смартфона: " );
        String inputShopForCheapSmartPhone = scan.nextLine ();
          for ( Shop shop : shops) {
            if (shop.hasNameShop(inputShopForCheapSmartPhone)) {
                shop.findCheapestSmartphone ();
                shop.outputInfoShop ();
          }
        }
        System.out.println("Введите название магазина для поиска в нем смартфона определенного бренда: ");
        String inputNameShopForProducer = scan.nextLine();
    System.out.print ( "Введите название бренда:" );
        String inputNameProducer = scan.nextLine();
        for (Shop shop : shops) {
            if (shop.hasNameShop(inputNameShopForProducer)) {
                shop.outputSmartPhoneByProducer(inputNameProducer);
            }


        }
    }
}

//1. Дан список смартфонов. Каждый смартфон имеет следующие характеристики:
//производитель (бренд)
//модель
//цена
//Необходимо найти:
//самый дешёвый смартфон
//смартфон по названию производителя
//смартфоны в ценовом интервале (пользователь вводит минимальную и максимальную цену).
//Найти - вывести всю информацию по смартфону.
//
//1.1 Дан список смартфонов (бренд, модель, цена) и магазинов (название, адрес), которые занимаются продажей смартфонов. Каждый магазин содержит разное кол-во смартфонов (от 2х до 6ти).
//Необходимо найти:
//самый дешёвый смартфон в магазине (название магазина вводит пользователь)
//смартфон по названию производителя в магазине (если его нет - вывести в консоль “смартфона нет в наличии”)
//смартфоны в ценовом интервале в определённом магазине (пользователь вводит название магазина, минимальную и максимальную цену)
//Найти - вывести всю информацию по смартфону и магазину, в котором он находится.
//
//1.2* Дан список смартфонов (бренд, модель, цена) и магазинов (название, адрес), которые занимаются продажей смартфонов. Каждый магазин содержит разное кол-во смартфонов (от 2х до 6ти).
//Необходимо найти:
//самый дешевый смартфон среди всех доступных в магазинах
//все смартфоны заданного производителя
//смартфон с заданной моделью, если таких несколько - то выбрать самый дешевый из них
//Найти - вывести всю информацию по смартфону и магазину, в котором он найден.