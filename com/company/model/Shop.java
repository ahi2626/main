package com.company.model;

public class Shop {
    private String name;
    private String address;
    private Smartphone[] smartphone;

    private int i;

    public Shop(String name, String address, Smartphone[] smartphone) {
        this.name = name;
        this.address = address;
        this.smartphone = smartphone;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }


    public void findCheapestSmartphone() {
        int cheapestSmartphone = 0;
        int priceCheapestSmartphone = Integer.MAX_VALUE;
        for ( int i = 0; i < smartphone.length; i++ ) {
            if (smartphone[i].getPrice () < priceCheapestSmartphone) {
                priceCheapestSmartphone = smartphone[i].getPrice ();
                cheapestSmartphone = i;
            }
        }
        outputInfoSmartphone ( cheapestSmartphone );
    }

    public void outputSmartPhoneByProducer(String nameProducer) {
        for ( Smartphone smartphone : smartphone ) {
            if (smartphone.hasNameProducer ( nameProducer )) {
                System.out.println ( "Смартфон: " + smartphone.getProducer () + " " + smartphone.getModel () + ". Его цена: " + smartphone.getPrice () );
                outputInfoShop ();
            }
        }
    }

    public void outputInfoSmartphone(int id) {
        System.out.println ( "Смартфон:" + smartphone[id].getProducer () + "" + smartphone[id].getModel () + ". Его цена" + smartphone[id].getPrice () );
    }


    public Boolean hasNameShop(String name) {
        return getName ().toLowerCase ().contains ( name.toLowerCase () );
    }

    public void outputInfoShop() {
        System.out.println ( "Название магазина: <<" + getName () + ">>. Его адресс: " + getAddress () );
    }

}






















//    public void cheapestSmartphone(String nameShopWithCheapestSmartphone, String addressWithCheapestSmartphone) {
//        int cheapestsmartphonePrice = smartphones[0].getPrice ();
//        for ( Smartphone cheapest : smartphones ) {
//            if (cheapest.getPrice () < cheapestsmartphonePrice) {
//                cheapestsmartphonePrice = cheapest.getPrice ();
//                System.out.println ( "Самый дешевый смарфтфон в магазине:" + cheapest.getProducer () + " " + cheapest.getModel () + ",цена" + cheapest.getPrice () + ".Можете преобрести в" + nameShopWithCheapestSmartphone + ", по адресу:" + addressWithCheapestSmartphone );
//            }
//        }
//    }


// public boolean hasName(String nameofproducer) {return nameofproducer.equalsIgnoreCase (model);
//  }

// public int choiceSmartphoneByName(String nameofproducer ) {
//   for (Smartphone cheap:smartphones ) {
//        if (smartphone.getName().equalsIgnoreCase (nameofproducer)) {
//         int result = smartphone.choiceSmartphoneByName(nameofproducer);
//          System.out.println(String.format( smartphone.getName(), result));
//         }


//     }
//      return 0;
//    }

