package com.company.model;

public class Smartphone {
    private String producer;
    private String model;
    private int price;


    public Smartphone(String producer, String model, int price) {
        this.producer = producer;
        this.model = model;
        this.price = price;
    }

    public String getProducer() {
        return producer;
    }



    public String getModel() {
        return model;
    }

    public int getPrice() {
        return price;
    }

    public boolean hasNameProducer(String phoneProducer) {
        return producer.equalsIgnoreCase ( phoneProducer );
    }

    public Boolean hasModel (String model){ return getModel().toLowerCase().contains(model.toLowerCase());

    }
}
// public boolean hasProducer(String nameofproducer) {return nameofproducer.equalsIgnoreCase (model);
//  }
