package com.company.data;


import com.company.model.Shop;
import com.company.model.Smartphone;

public final class Generator {
    public Generator() {
    }

    public static Shop[] generate() {
        Shop[] shops = new Shop[3];
        {
            Smartphone[] smartphone = {
                    new Smartphone ( "Apple", "XI", 20000 ),
                    new Smartphone ( "Samsung", "Galaxy", 18000 ),
                    new Smartphone ( "Nokia", "3310", 5000 )
            };
            shops[0] = new Shop ( "Mobyluck", "Клочковская 70", smartphone );
        }
        {
            Smartphone[] smartphone = {
                    new Smartphone ( "Apple", "XI", 21000 ),
                    new Smartphone ( "Samsung", "Galaxy S10", 19000 ),
                    new Smartphone ( "Nokia", "3310", 4500 ),
                    new Smartphone ( "Huawei", "D200", 6000 ),
                    new Smartphone ( "Xaoimi", "P100", 7000 )
            };
            shops[1] = new Shop ( "Allo", "Героев Труда 7", smartphone );
        }
        {
            Smartphone[] smartphone = {
                    new Smartphone ( "Apple", "XI", 23000 ),
                    new Smartphone ( "Samsung", "Galaxy S10", 20100 ),
                    new Smartphone ( "Nokia", "Gold", 5500 )
            };
            shops[2] = new Shop ( "Citrus", "Сумская 70", smartphone );
        }
        return shops;
    }
}
